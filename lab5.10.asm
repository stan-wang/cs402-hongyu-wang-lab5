.data
.align 0
char1: .byte 'a'
word1: .word 0x89abcdef
char2: .byte 'b'
word2: .word 0
.text
.globl main
main:
lui $at, 0x1000
ori $a0, $at, 1
lui $at, 0x1000
ori $a1, $0, 6
lwl $t0, 3($a0)
lwr $t0, 0($a0)
swl $t0, 3($a0)
swr $t0, 0($a0)
sw 0($a0), word2
jr $ra