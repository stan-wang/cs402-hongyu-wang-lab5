.data 0x10000000
char1: .byte 'a'
double1: .double 1.1
char2: .byte 'b'
half1: .half 0x8001
char3: .byte 'c'
word1: .word 0x56789abc
char4: .byte 'd'
word2: .word 0
.text
.globl main
main:
la $v0, word1
lb $t0, 0($v0)
lb $t1, 1($v0)
lb $t2, 2($v0)
lb $t3, 3($v0)
lbu $t4, 0($v0)
lbu $t5, 1($v0)
lbu $t6, 2($v0)
lbu $t7, 3($v0)
lb $t8, half1
lbu $t9, half2
la $v1, word2
sb $t3, 0($v0)
sb $t2, 1($v0)
sb $t1, 2($v0)
sb $t0, 3($v0) 
jr $ra