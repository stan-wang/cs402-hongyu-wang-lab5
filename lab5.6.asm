.data 0x10000000
user1: .word 0
msg1: .asciiz "Please enter an integer number: "
msg2: .asciiz "If bytes were layed in reverse order the number would be: "
.text
.globl main
main:
addi $sp, $sp, -4
sw $ra, 4($sp)
la $a0, msg1
li $v0, 5
syscall
sw $v0, user1($0)
la $a0, user1
jal Reverse_bytes
la $a0, msg2
li $v0, 4
syscall
lw $a0, user1($0)
li $v0, 1
syscall
lw $ra, 4($sp)
addi $sp, $sp, 4
jr $ra
Reverse_bytes:
lb $t0, 0($a0)
lb $t1, 3($a0)
sb $t0, 3($a0)
sb $t1, 0($a0)
lb $t0, 1($a0)
lb $t1, 2($a0)
sb $t0, 2($a0)
sb $t1, 1($a0)
jr $ra